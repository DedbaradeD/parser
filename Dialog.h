#pragma once

#include <QDialog>
#include <QFile>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();

    QStringList getFilesPaths();

private slots:
    void addFile();
    void deleteFile();
    void setFilesPaths();

signals:
    void addWordWindow();
    void getResult();

private:
    Ui::Dialog *_ui;
    QStringList _filesPathsList;
};
