#pragma once

#include <gtest/gtest.h>
#include "Controller.h"

class Dialog;

class Tests : public ::testing::Test
{
protected:
    void SetUp()
    {
        _controllerObj = new Controller(new Dialog());
    }
    void TearDown()
    {
        delete _controllerObj;
    }
    Controller *_controllerObj;
};

TEST_F(Tests, OpeningReadingFile)
{
    ASSERT_EQ(true, _controllerObj->openReadingFile("test.txt"));
}

TEST_F(Tests, ReadingFile)
{
    ASSERT_EQ(true, _controllerObj->readFile());
}

TEST_F(Tests, OpeningWritingFile)
{
    ASSERT_EQ(true, _controllerObj->openWritingFile(0));
}

TEST_F(Tests, WritingFile)
{
    ASSERT_EQ(true, _controllerObj->writeFile());
}
