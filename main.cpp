#include "Dialog.h"
#include <QApplication>
#include <QTextCodec>
#include <gtest/gtest.h>
#include <Tests.h>
#include <Controller.h>

int main(int argc, char *argv[])
{
    QTextCodec *cyrillicCodec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(cyrillicCodec);
    ::testing::InitGoogleTest(&argc, argv);
    QApplication app(argc, argv);
    Dialog dialogWindow;
    Controller controller(&dialogWindow);
    dialogWindow.show();
//    return RUN_ALL_TESTS();
    return app.exec();
}
