#pragma once

#include <QDialog>

namespace Ui {
class AddWord;
}

class AddWord : public QDialog
{
    Q_OBJECT

public:
    explicit AddWord(QWidget *parent = nullptr);
    ~AddWord();

    QString getWord();

private:
    Ui::AddWord *_ui;
    QString _word;

private slots:
    void saveWord();
    void cancel();
};
