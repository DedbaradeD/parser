#-------------------------------------------------
#
# Project created by QtCreator 2016-12-27T01:05:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Parser
TEMPLATE = app


SOURCES += main.cpp\
        Dialog.cpp \
    AddWord.cpp \
    Controller.cpp

HEADERS  += Dialog.h \
    AddWord.h \
    Tests.h \
    Controller.h

FORMS    += Dialog.ui \
    AddWord.ui

LIBS += -L/usr/local/lib -lgtest
