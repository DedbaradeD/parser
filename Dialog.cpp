#include "Dialog.h"
#include "ui_Dialog.h"
#include <QString>
#include <QFileDialog>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    _ui(new Ui::Dialog)
{
    _ui->setupUi(this);

    _ui->saveFileButton->setEnabled(false);
    _ui->addWordButton->setEnabled(false);
    _ui->deleteFileButton->setEnabled(false);

    connect(_ui->addFileButton, &QPushButton::clicked, this, &Dialog::addFile);
    connect(_ui->deleteFileButton, &QPushButton::clicked, this, &Dialog::deleteFile);
    connect(_ui->saveFileButton, &QPushButton::clicked, this, &Dialog::setFilesPaths);
    connect(_ui->addWordButton, &QPushButton::clicked, this, &Dialog::addWordWindow);
}

Dialog::~Dialog()
{
    delete _ui;
}

void Dialog::addFile()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Open Document"),
                        QDir::currentPath(), tr("All files (*.*)"));
    if (filename != "")
    {
        _ui->filesListWidget->addItem(filename);
        _ui->deleteFileButton->setEnabled(true);
        _ui->addWordButton->setEnabled(true);
        _ui->saveFileButton->setEnabled(true);
    }
}

void Dialog::deleteFile()
{
    delete _ui->filesListWidget->currentItem();
}

void Dialog::setFilesPaths()
{
    for(int i = 0; i < _ui->filesListWidget->count(); i++)
        if(_ui->filesListWidget->item(i)->text() != "")
            _filesPathsList.append(_ui->filesListWidget->item(i)->text());
    emit this->getResult();
}

QStringList Dialog::getFilesPaths()
{
    return _filesPathsList;
}
