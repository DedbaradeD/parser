#include "AddWord.h"
#include "ui_AddWord.h"

AddWord::AddWord(QWidget *parent) :
    QDialog(parent),
    _ui(new Ui::AddWord)
{
    _ui->setupUi(this);

    connect(_ui->saveButton, &QPushButton::clicked, this, &AddWord::saveWord);
    connect(_ui->cancelButton, &QPushButton::clicked, this, &AddWord::cancel);
}

AddWord::~AddWord()
{
    delete _ui;
}

void AddWord::saveWord()
{
    _word = _ui->lineEdit->text();
    this->close();
}

void AddWord::cancel()
{
    this->close();
}

QString AddWord::getWord()
{
    return _word;
}
