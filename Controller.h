#pragma once

#include "Dialog.h"
#include "AddWord.h"
#include <QObject>
#include <QFile>

class QString;
class QStringList;

class Controller : public QObject
{
    Q_OBJECT

public:
    explicit Controller(Dialog *dialogWindow);
    ~Controller();

    bool openReadingFile(QString filePath);
    bool readFile();
    bool openWritingFile(int fileCount);
    bool writeFile();

private slots:
    void parseFile();
    void showAddWordWindow();

private:
    Dialog *_dialogWindow;
    AddWord *_addWordWindow;
    QStringList _textLinesList;
    QFile _file;
    QFile _resultFile;
    int _fileCount;
};
