#include "Controller.h"
#include "Dialog.h"
#include <QTextStream>
#include <QDebug>
#include <QTextCodec>
#include <QMessageBox>

Controller::Controller(Dialog *dialogWindow):
    _dialogWindow(dialogWindow)
{
    _addWordWindow = new AddWord();

    QObject::connect(_dialogWindow, &Dialog::getResult, this, &Controller::parseFile);
    QObject::connect(_dialogWindow, &Dialog::addWordWindow, this, &Controller::showAddWordWindow);
}

Controller::~Controller()
{

}

void Controller::showAddWordWindow()
{
    _addWordWindow->show();
}

bool Controller::openReadingFile(QString filePath)
{
    _file.setFileName(filePath);
    if (_file.open(QIODevice::ReadOnly))
        return true;
    else
    {
        qDebug() << "Opening file error";
        return false;
    }
}

bool Controller::readFile()
{
    QTextStream readFileStream(&_file);
    if(readFileStream.status() != QTextStream::Ok)
    {
        qDebug() << "Reading file error";
        return false;
    }
    else
    {
        readFileStream.setCodec(QTextCodec::codecForName("Windows-1251"));
        while (!readFileStream.atEnd())
            _textLinesList << readFileStream.readLine();
        return true;
    }
}

bool Controller::openWritingFile(int fileCount)
{
    _resultFile.setFileName("result" + QString::number(fileCount) + ".txt");
    if (_resultFile.open(QIODevice::WriteOnly))
        return true;
    else
    {
        qDebug() << "Opening file error";
        return false;
    }
}

bool Controller::writeFile()
{
    QTextStream writeFileStream(&_resultFile);
    if(writeFileStream.status() != QTextStream::Ok)
    {
        qDebug() << "Writing file error";
        return false;
    }
    else
    {
        QString resultText = _textLinesList.join("\n");
        writeFileStream << resultText;
        return true;
    }
}

void Controller::parseFile()
{
    const QString word = _addWordWindow->getWord();
    if (!word.isEmpty())
    {
        _fileCount = 0;
        for (auto path : _dialogWindow->getFilesPaths())
        {
            openReadingFile(path);
            readFile();

            for (int i = 0; i < _textLinesList.count(); i++)
            {
                auto line = _textLinesList.value(i);
                line.remove(word);
                _textLinesList.replace(i, line);
            }

            _textLinesList.sort(Qt::CaseInsensitive);

            this->openWritingFile(_fileCount);
            this->writeFile();

            _file.close();
            _resultFile.close();

            _fileCount++;
        }
        QMessageBox operationCompleted;
        operationCompleted.setText("Operation completed successfully.");
        operationCompleted.exec();
    }
    else
    {
        QMessageBox emptyWord;
        emptyWord.setText("Please add the word.");
        emptyWord.exec();
    }
}
